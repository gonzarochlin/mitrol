// Esto es un ejemplo de un reducer modificar a gusto

export const ADD = "ADD"
export const REDUCE = "REDUCE"

export const add = () => ({type : ADD })
export const reduce = () => ({type : REDUCE })

const initState = { value : 0 }

export const countReducer = ( state = initState , action ) => {
  switch (action.type){
    case ADD:
      return {value : state.value + 1};
    case REDUCE:
      return {value : state.value - 1};
    default:
      return state
  }
}