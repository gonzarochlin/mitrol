export const ADD_TAREA = "ADD_TAREA"
export const REMOVE_TAREA = "REMOVE_TAREA"
export const GET_TAREAS = "GET_TAREAS"

let nextIdTarea = 0

export const add = text => ({
  type : ADD_TAREA,
  id: nextIdTarea++,
  text
})

export const remove = id => ({
    type: REMOVE_TAREA,
    id
})

export const get = () => ({
    type: GET_TAREAS
})


export const tareasReducer = ( state = [] , action ) => {
  switch (action.type){
    case ADD_TAREA:
      return [
        ...state,
        {
            id: action.id,
            text: action.text
        }
      ];
    case REMOVE_TAREA:
      return state.filter(tarea => {
        return tarea.id !== action.id;
      })
    case GET_TAREAS:
      return state
    default:
      return state
  }
}