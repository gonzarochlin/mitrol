import { createStore, combineReducers } from "redux";
import { countReducer } from "./countReducer";
import { tareasReducer } from "./tareasReducer";

const reducers = combineReducers({
  count : countReducer,
  tareas: tareasReducer
  // Agregar los reducers que creas necesarios
},)

export default createStore(reducers)