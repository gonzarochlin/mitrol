import React, { useState } from 'react'
import { connect } from 'react-redux'
import { add, remove } from '../store/tareasReducer'
import TareasList from '../components/TareasList';

function Administrador(props) {
    const [value, setValue] = useState('');

    function onClickAgregar() {
        // const {value} = this.state
        if (value.trim() === ''){
            return
        }
        props.add(value)
        setValue('')
    }

    function handleInputChange(event) {
        setValue(event.target.value);
    }

    return (
        <>
            <div className="input-group mb-3">
                <input type="text" className="form-control" value={value} onChange={handleInputChange} />
                <div className="input-group-append">
                    <button className="btn btn-outline-primary" type="button" id="button-addon2" onClick={onClickAgregar}>
                    Agregar
                    </button>
                </div>
            </div> 
            <TareasList tareas={props.tareas} removeTarea={props.remove} />
        </>
    )
}

const mapStateToProps = state => ({
    tareas: state.tareas
})

const mapDispatchToProps = dispatch => ({
    remove: id => dispatch(remove(id)),
    add: text => dispatch(add(text)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Administrador)