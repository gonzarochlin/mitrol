import React from 'react'
import { connect } from 'react-redux'
import { add, reduce } from '../store/countReducer'

class Contador extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            autoAddEnabled: false,
            timer: 1,
        };
    }

    counterDown = () => {
        this.props.reduce()
    }

    counterUp = () => {
        this.props.add()
    }

    timerDown = () => {
        setTimeout(this.counterDown, this.state.timer * 1000)
    }

    timerUp = () => {
        setTimeout(this.counterUp, this.state.timer * 1000)
    }

    onChangeAutoAdd = () => {
        const {autoAddEnabled} = this.state
        this.setState({autoAddEnabled: !autoAddEnabled});
    }

    onChangeTimer = (event) => {
        this.setState({timer: event.target.value});
    }

    autoAdd = () => {
        if(this.timeout){
            clearTimeout(this.timeout)
            this.timeout = null;
        }
        if(this.state.autoAddEnabled){
            this.timeout = setTimeout(this.counterUp, this.state.timer * 1000)
        }
    }

    render() {   
        this.autoAdd()
        return (
            <>
                <div className="input-group mb-3">
                    <div className="input-group-append">
                        <button className="btn btn-outline-primary" type="button" id="button-addon2" onClick={this.timerDown}>
                            -1 (1 seg)
                        </button>
                    </div>
                    <div className="input-group-append">
                        <button className="btn btn-outline-primary" type="button" id="button-addon2" onClick={this.counterDown}>
                            -1
                        </button>
                    </div>
                    <input type="number" className="form-control" value={this.props.count} readOnly/>
                    <div className="input-group-append">
                        <button className="btn btn-outline-primary" type="button" id="button-addon2" onClick={this.counterUp}>
                            +1
                        </button>
                    </div>
                    <div className="input-group-append">
                        <button className="btn btn-outline-primary" type="button" id="button-addon2" onClick={this.timerUp}>
                            +1 (1 seg)
                        </button>
                    </div>
                </div> 
                <div className="d-inline-block">
                    <div className="custom-control custom-switch">
                        <input type="checkbox" className="custom-control-input" id="autoAdd" value={this.state.autoAdd} onChange={this.onChangeAutoAdd}/>
                        <label className="custom-control-label" for="autoAdd">Incrementar automáticamente</label>
                    </div>
                </div>
                <div className="d-inline-block mx-2">cada</div>
                <div className="d-inline-block col-4">
                    <div className="input-group mb-3">
                        <input type="number" className="form-control" value={this.state.timer} min={0} onChange={this.onChangeTimer}/>
                        <div className="input-group-append">
                            <span className="input-group-text">seg</span>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

const mapStateToProps = state => ({
    count: state.count.value
})

const mapDispatchToProps = dispatch => ({
    add: () => dispatch(add()),
    reduce: () => dispatch(reduce()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Contador)