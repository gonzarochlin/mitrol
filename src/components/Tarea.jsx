import React from 'react'

const Tarea = ({ onClick, text }) => (
    <li className="list-group-item">
        {text}
        <button className="float-right btn btn-link text-danger" onClick={onClick}>
            &times;
        </button>
    </li>
)

export default Tarea