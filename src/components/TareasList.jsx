import React from 'react'
import Tarea from './Tarea'

const TareasList = ({ tareas, removeTarea }) => (
    <ul className="list-group">
        {tareas.map(tarea =>
            <Tarea
                key={tarea.id}
                {...tarea}
                onClick={() => removeTarea(tarea.id)}
            />
        )}
    </ul>
)

export default TareasList